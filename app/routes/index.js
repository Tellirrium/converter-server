import Router from 'koa-router';

import { getImage } from '../services/imageConverter/index.js';

export const router = new Router();

router.get('/image', async (ctx) => {
  try {
    ctx.body = await getImage(ctx.query);
  }
  catch (err) {
    console.log('err', err);
    ctx.status = err.status || 500;
    ctx.body = err.message;
  }
});
  