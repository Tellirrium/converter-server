import Koa from 'koa';

import middlewares from './middlewares/index.js';
import { router } from './routes/index.js';

const app = new Koa();

middlewares(app);

app.on('error', (err) => console.log(err));
app.use(router.routes());
app.use(router.allowedMethods());

export default app;
