import cors from '@koa/cors';
import bodyParser from 'koa-bodyparser';

export default (app) => {
  app.use(bodyParser());

  app.use(cors());

  app.use(async (ctx, next) => {
    try {
      await next();
    }
    catch (error) {
      ctx.status = error.status || 500;
      ctx.body = error.message;
      ctx.app.emit('error', error, ctx);
    }
  });

  app.use(async (ctx, next) => {
    ctx.set('Cache-Control', 'max-age=60');
    await next();
  });
};
