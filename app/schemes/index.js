import Joi from 'joi';

export const requestParams = Joi.object({
  width: Joi.string().pattern(/^[0-9]+$/, 'numbers').required(),
  height: Joi.string().pattern(/^[0-9]+$/, 'numbers').required(),
  name: Joi.string().min(1).max(30).required(),
});
