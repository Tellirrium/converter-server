import fs from 'fs';

class FileSystem {
  constructor() {
    this.fileSystem = fs;
  }
}

export default FileSystem;
