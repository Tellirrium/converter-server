import FileSystem from '../index.js';

class File extends FileSystem {
  constructor() {
    super();
  }

  getReadStream(path) {
    if (typeof this.fileSystem?.createReadStream !== 'function') {
      throw new Error('Server error, try later.');
    }

    return this.fileSystem.createReadStream(path);
  }
}

export default new File();
