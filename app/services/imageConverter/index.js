import graphicsMagick from 'gm';
import fs from 'fs';
import path from 'path';
import { promisify } from 'util';

import * as schemes from '../../schemes/index.js';
import File from '../../accessData/Models/file.js';

export function getImage(params) {
  return new Promise(async (resolve, reject) => {
    try {
      const { width, height, name: fileName } = await schemes.requestParams.validateAsync(params);
      const pathToFile = path.join(path.resolve(), `/app/images/${fileName}.jpg`);
      const ReadableStream = File.getReadStream(pathToFile);
      const chunks = [];

      ReadableStream.on('error', (error) => {
        if (error.syscall === 'open') {
          reject(new Error(`Can not find a file with name ${fileName}`));
        }
      });

      ReadableStream.on('data', (chunk) => {
        chunks.push(chunk);
      });

      ReadableStream.on('end', async () => {
        const buf = Buffer.concat(chunks);
        const gm = graphicsMagick(buf, 'image.jpg');
        const getSize = promisify(gm.size.bind(gm));
        const actualSize = await getSize();

        if (width * height > actualSize.width * actualSize.height) {
          return reject(
            new Error('The sizes you requested are larger than the actual size of the picture.')
          );
        }

        gm
        .resize(width, height)
        .stream((err, stdout, stderr) => {
          if (err) {
            return reject(err);
          }

          resolve(stdout);
        });
      });
    }
    catch (err) {
      reject(err);
    }
  });
}
