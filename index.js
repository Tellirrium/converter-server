import dotenv from 'dotenv';

dotenv.config();

import app from './app/index.js';
import { createServer } from 'http';

createServer(app.callback());

const port = Number(process.env.PORT);

(async () => {
  try {
    app.listen(port, () => {
      console.log(`The app has been started on ${port} port.`);
    });
  } catch (error) {
    console.log(error);
  }
})();
